from django.db import models
from ..students.models import Student


class Grade(models.Model):
    student = models.ForeignKey(Student)

    value = models.PositiveIntegerField(
        null=False
    )

    def details(self):
        return {
            'student': self.student.name + ' ' + self.student.last_name,
            'value': self.value
        }
