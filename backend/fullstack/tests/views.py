from django.http import JsonResponse, HttpResponse
from .models import Test
from django.http import QueryDict


def manage(request):
    if request.method == 'GET':
        return JsonResponse(Test.all_tests(), safe=False)
    elif request.method == 'POST':
        return JsonResponse(Test.create_or_edit(None, request.POST), safe=False)
    elif request.method == 'PATCH':
        patch_data = QueryDict(request.body)
        return JsonResponse(Test.create_or_edit(patch_data['id'], patch_data), safe=False)
    else:
        return HttpResponse(status=405)


def view(request, id):
    if request.method == 'GET':
        t = Test.objects.get(id=id)
        return JsonResponse(t.details(True), safe=False)
    else:
        return HttpResponse(status=405)


def grade(request):
    if request.method == 'POST':
        return JsonResponse(Test.add_grade(request.POST), safe=False)
    else:
        return HttpResponse(status=405)


def delete(request, id):
    t = Test.objects.get(id=id)
    t.delete()
    return HttpResponse(status=200)
