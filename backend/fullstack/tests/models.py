from django.db import models
from ..courses.models import Course
from ..grades.models import Grade


class Test(models.Model):
    name = models.TextField(
        max_length=80,
        null=False
    )

    course = models.ForeignKey(Course)

    grades = models.ManyToManyField(Grade)

    def details(self, with_grades_and_students=False):
        out = {
            'id': self.id,
            'name': self.name,
            'course': self.course.name
        }

        if with_grades_and_students:
            out['grades'] = [g.details() for g in self.grades.all()]
            out['students'] = [s.details() for s in self.course.students.all()]

        return out

    @staticmethod
    def add_grade(gr_data):
        t = Test.objects.get(pk=int(gr_data['test_id']))
        g = Grade.objects.create(student_id = int(gr_data['student_id']), value=int(gr_data['value']))
        return t.grades.add(g)

    @staticmethod
    def all_tests():
        return {
          'type': 'test',
          'head': ['Id', 'Name', 'Course'],
          'body': [x.details() for x in Test.objects.all()]
        }

    @staticmethod
    def create_or_edit(id, data):
        if id is None:
            t = Test()
        else:
            t = Test.objects.get(id=id)

        t.name = data['name']
        t.course_id = data['course_id']
        return t.save()
