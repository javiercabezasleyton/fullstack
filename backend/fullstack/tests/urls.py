from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.manage),
    url(r'^grade/$', views.grade),
    url(r'^delete/(?P<id>\d+)$', views.delete),
    url(r'^(?P<id>\d+)/$', views.view),
]
