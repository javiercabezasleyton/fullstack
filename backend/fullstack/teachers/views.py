from django.http import JsonResponse, HttpResponse
from .models import Teacher
from django.http import QueryDict


def manage(request):
    if request.method == 'GET':
        return JsonResponse(Teacher.all_teachers(), safe=False)
    elif request.method == 'POST':
        return JsonResponse(Teacher.create_or_edit(None, request.POST), safe=False)
    elif request.method == 'PATCH':
        patch_data = QueryDict(request.body)
        return JsonResponse(Teacher.create_or_edit(patch_data['id'], patch_data), safe=False)
    else:
        return HttpResponse(status=405)


def view(request, id):
    if request.method == 'GET':
        t = Teacher.objects.get(id=id)
        return JsonResponse(t.details(), safe=False)
    else:
        return HttpResponse(status=405)


def delete(request, id):
    t = Teacher.objects.get(pk=int(id))
    t.delete()
    return HttpResponse(status=200)
