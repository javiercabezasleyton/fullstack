from django.apps import AppConfig


class TeachersConfig(AppConfig):
    name = 'fullstack.teachers'
