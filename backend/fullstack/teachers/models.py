from django.db import models


class Teacher(models.Model):
    name = models.TextField(
        max_length=80,
        null=False
    )

    last_name = models.TextField(
        max_length=80,
        null=False
    )

    def details(self):
        return {
            'id': self.id,
            'name': self.name,
            'last_name': self.last_name
        }

    def __str__(self):
        return self.name + ' ' + self.last_name


    @staticmethod
    def all_teachers():
        """
        Returns a dictionary with all the teachers on the database in the format
        'head': List of fields that will be returned
        'body': Each of the entries of this model
        :return:
        """
        all_teachers = Teacher.objects.all()

        return {
          'type': 'teacher',
          'head': ['Id', 'Name', 'Last Name'],
          'body': [x.details() for x in all_teachers]
        }

    @staticmethod
    def create_or_edit(id, data):
        if id is None:
            t = Teacher()
        else:
            t = Teacher.objects.get(id=id)

        t.name = data['name']
        t.last_name = data['last_name']
        return t.save()
