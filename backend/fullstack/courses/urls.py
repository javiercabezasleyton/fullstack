from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.manage),
    url(r'^(?P<id>\d+)/$', views.view),
    url(r'^add/(?P<course_id>\d+)/(?P<student_id>\d+)/$', views.add),
    url(r'^remove/(?P<course_id>\d+)/(?P<student_id>\d+)/$', views.remove),
    url(r'^delete/(?P<id>\d+)$', views.delete)
]
