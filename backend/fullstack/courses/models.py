from django.db import models
from ..teachers.models import Teacher
from ..students.models import Student


class Course(models.Model):
    name = models.TextField(
        max_length=50,
        null=False
    )

    teacher = models.ForeignKey(Teacher)

    students = models.ManyToManyField(Student)

    def details(self, with_students=False):
        out = {
                'id': self.id,
                'name': self.name,
                'teacher_name': str(self.teacher)
        }

        if with_students:
            out['students'] = [s.details() for s in self.students.all()]

        return out

    @staticmethod
    def all_courses():
        return {
          'type': 'course',
          'head': ['Id', 'Name', 'Teacher'],
          'body': [x.details() for x in Course.objects.all()]
        }

    @staticmethod
    def create_or_edit(id, data):
        if id is None:
            c = Course()
        else:
            c = Course.objects.get(id=id)

        c.name = data['name']
        c.teacher_id = data['teacher_id']

        return c.save()
