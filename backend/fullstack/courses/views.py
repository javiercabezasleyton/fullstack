from django.http import JsonResponse, HttpResponse
from django.http import QueryDict
from .models import Course
from ..students.models import Student


def manage(request):
    if request.method == 'GET':
        return JsonResponse(Course.all_courses(), safe=False)
    elif request.method == 'POST':
        return JsonResponse(Course.create_or_edit(None, request.POST), safe=False)
    elif request.method == 'PATCH':
        patch_data = QueryDict(request.body)
        return JsonResponse(Course.create_or_edit(patch_data['id'], patch_data), safe=False)
    else:
        return HttpResponse(status=405)


def view(request, id):
    s = Course.objects.get(id=id)
    return JsonResponse(s.details(True), safe=False)


def add(request, course_id, student_id):
    c = Course.objects.get(pk=int(course_id))
    s = Student.objects.get(pk=int(student_id))
    return JsonResponse(c.students.add(s), safe=False)


def remove(request, course_id, student_id):
    c = Course.objects.get(pk=int(course_id))
    s = Student.objects.get(pk=int(student_id))
    return JsonResponse(c.students.remove(s), safe=False)


def delete(request, id):
    c = Course.objects.get(pk=int(id))
    c.delete()
    return HttpResponse(status=200)
