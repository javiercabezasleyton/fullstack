from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.manage),
    url(r'^(?P<id>\d+)/$', views.view),
    url(r'^delete/(?P<id>\d+)$', views.delete)
]
