from django.apps import AppConfig


class StudentsConfig(AppConfig):
    name = 'fullstack.students'
