from django.db import models


class Student(models.Model):
    name = models.TextField(
        max_length=80,
        null=False
    )

    last_name = models.TextField(
        max_length=80,
        null=False
    )

    def details(self):
        return {
            'id': self.id,
            'name': self.name,
            'last_name': self.last_name
        }

    @staticmethod
    def all_students():
        return {
          'type': 'student',
          'head': ['Id', 'Name', 'Last Name'],
          'body': [x.details() for x in Student.objects.all()]
        }

    def create_or_edit(id, data):
      if id is None:
        s = Student()
      else:
        s = Student.objects.get(id=id)

      s.name = data['name']
      s.last_name = data['last_name']
      return s.save()

