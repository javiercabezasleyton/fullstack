from django.http import JsonResponse, HttpResponse
from django.http import QueryDict
from .models import Student


def manage(request):
    if request.method == 'GET':
        return JsonResponse(Student.all_students(), safe=False)
    elif request.method == 'POST':
        return JsonResponse(Student.create_or_edit(None, request.POST), safe=False)
    elif request.method == 'PATCH':
        patch_data = QueryDict(request.body)
        return JsonResponse(Student.create_or_edit(patch_data['id'], patch_data), safe=False)
    else:
        return HttpResponse(status=405)

def view(request, id):
    s = Student.objects.get(id=id)
    return JsonResponse(s.details(), safe=False)


def delete(request, id):
    s = Student.objects.get(id=id)
    s.delete()
    return HttpResponse(status=200)
