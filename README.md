# Ejercicios:

1) Escriba una Query que entregue la lista de alumnos para el curso "programación"

>SELECT s.name, s.last_name

>FROM courses_course_students as cs

>JOIN students_student as s ON s.id = cs.student_id

>WHERE cs.course_id = c_id

En donde c_id es el identificador del curso.

2) Escriba una Query que calcule el promedio de notas de un alumno en un curso.

>SELECT SUM(g.value) / COUNT(g.value) as grade

>FROM tests_test_grades AS tg

>INNER JOIN grades_grade AS g ON tg.grade_id = g.id

>INNER JOIN tests_test AS t ON t.id = tg.test_id

>INNER JOIN courses_course as c ON c.id = t.course_id

>WHERE t.course_id = 1  and g.student_id = 4

En donde las variables son el identificador del curso y el id del alumno.


3) Escriba una Query que entregue a los alumnos y el promedio que tiene en cada curso.

> SELECT cs.student_id, t.course_id, SUM(g.value) / COUNT(g.value) as grade

> FROM tests_test_grades AS tg

> INNER JOIN grades_grade AS g ON tg.grade_id = g.id

> INNER JOIN tests_test AS t ON t.id = tg.test_id

> INNER JOIN courses_course AS c ON c.id = t.course_id

> INNER JOIN courses_course_students AS cs ON cs.course_id = c.id

> GROUP BY cs.student_id, t.course_id



4) Escriba una Query que lista a todos los alumnos con más de un curso con promedio rojo.

Se reutilizaría la anterior, agregándole un where según la nota.

>SELECT *

>FROM (

>    QUERY_ANTERIOR

>)as student_course_grade

>WHERE student_course_grade.grade < 55


Entonces quedaría:

> SELECT *

> FROM (

>   SELECT cs.student_id, t.course_id, SUM(g.value) / COUNT(g.value) as grade

>   FROM tests_test_grades AS tg

>   INNER JOIN grades_grade AS g ON tg.grade_id = g.id

>   INNER JOIN tests_test AS t ON t.id = tg.test_id

>   INNER JOIN courses_course AS c ON c.id = t.course_id

>   INNER JOIN courses_course_students AS cs ON cs.course_id = c.id

>   GROUP BY cs.student_id, t.course_id

> )as student_course_grade

> WHERE student_course_grade.grade < 55



5) Dejando de lado el problema del cólegio se tiene una tabla con información de jugadores de tenis: PLAYERS(Nombre, Pais, Ranking). Suponga que Ranking es un número de 1 a 100 que es distinto para cada jugador. Si la tabla en un momento dado tiene solo 20 registros, indique cuantos registros tiene la tabla que resulta de la siguiente consulta:

Según yo deberían de ser 19 consultas dado que se usa mayor extricto, ahora, dado que no está en las alternativas voy a suponer
que mi idea está incorrecta.

# Supuestos:
* Las notas son enteros positivos.
* Uso escala 0-100.

# Como correr:
La aplicación consiste en un frontend VueJS con un Backend django. Por lo mismo se deben de correr
ambos sistemas de forma separada.

En el directorio fullstack:
1) npm install
2) npm run dev
3) cd backend
4) python3 manage.py runserver
5) La aplicación está configurada para funcionar con las direcciones por defecto de tanto VueJS
como django, que son http://localhost:8080 y http://localhost:8000 respectivamente. Si se elije otra debe de hacer el cambio
en el los archivos fullstack/main.js (función url_backend) y
fullstack/backend/settings.py variable FRONTEND_URL.


Consideraciones y otros:
* Por asuntos de tiempo no hice validaciones de ingreso de datos, por lo
que confío en el input del usuario (que es feo).
* Supongo que en la sección de "timetable" el input está con un json
en formato correcto y que las horas no se pisan.
* El ejercicio de la agenda está en la sección timetable de la aplicación.
* Se pueden agregar notas desde la vista "View" de una prueba.
* Se pueden agregar alumnos a un curso desde la vista "View" de un curso.
* Me hubiese gustado implementar un mejor sistema de borrado (de que, por ejemplo, te
advirtiese si borrabas una prueba con notas) pero no me dio el tiempo para hacerlo. Ahora simplemente
borra todo.
* Encontré un bug muy tarde (que no alcancé a arreglar) de que, para entidades que usan dropdown (Ej: Para la elección de profesor para un curso)
al hacer edit, el dropdown parte vacío.
