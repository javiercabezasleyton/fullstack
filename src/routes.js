const Home = resolve => {
    require.ensure(['./sections/MainContent.vue'], () => {
        resolve(require('./sections/MainContent.vue'));
    });
};

const Timetable = resolve => {
    require.ensure(['./sections/timetable/Main.vue'], () => {
        resolve(require('./sections/timetable/Main.vue'));
    });
};

const Teacher = resolve => {
    require.ensure(['./sections/teacher/Main.vue'], () => {
        resolve(require('./sections/teacher/Main.vue'));
    });
};

const TeacherEdit = resolve => {
    require.ensure(['./sections/teacher/Edit.vue'], () => {
        resolve(require('./sections/teacher/Edit.vue'));
    });
};

const TeacherCreate = resolve => {
    require.ensure(['./sections/teacher/Create.vue'], () => {
        resolve(require('./sections/teacher/Create.vue'));
    });
};

const Student = resolve => {
    require.ensure(['./sections/student/Main.vue'], () => {
        resolve(require('./sections/student/Main.vue'));
    });
};

const StudentCreate = resolve => {
    require.ensure(['./sections/student/Create.vue'], () => {
        resolve(require('./sections/student/Create.vue'));
    });
};

const StudentEdit = resolve => {
    require.ensure(['./sections/student/Edit.vue'], () => {
        resolve(require('./sections/student/Edit.vue'));
    });
};

const Course = resolve => {
    require.ensure(['./sections/course/Main.vue'], () => {
        resolve(require('./sections/course/Main.vue'));
    });
};

const CourseCreate = resolve => {
    require.ensure(['./sections/course/Create.vue'], () => {
        resolve(require('./sections/course/Create.vue'));
    });
};

const CourseView = resolve => {
    require.ensure(['./sections/course/View.vue'], () => {
        resolve(require('./sections/course/View.vue'));
    });
};

const CourseEdit = resolve => {
    require.ensure(['./sections/course/Edit.vue'], () => {
        resolve(require('./sections/course/Edit.vue'));
    });
};

const Test = resolve => {
    require.ensure(['./sections/test/Main.vue'], () => {
        resolve(require('./sections/test/Main.vue'));
    });
};

const TestView = resolve => {
    require.ensure(['./sections/test/View.vue'], () => {
        resolve(require('./sections/test/View.vue'));
    });
};


const TestCreate = resolve => {
    require.ensure(['./sections/test/Create.vue'], () => {
        resolve(require('./sections/test/Create.vue'));
    });
};

const TestEdit = resolve => {
    require.ensure(['./sections/test/Edit.vue'], () => {
        resolve(require('./sections/test/Edit.vue'));
    });
};


export const routes = [
    { path: '', component: Home, name: 'home'},
    { path: '/timetable', component: Timetable, name: 'timetable'},
    { path: '/teacher', component: Teacher, name: 'teacher'},
    { path: '/teacher', component: Teacher, name: 'teacher_view'},
    { path: '/teacher/new', component: TeacherCreate, name: 'teacher_create'},
    { path: '/teacher/edit/:id', component: TeacherEdit, name: 'teacher_edit'},
    { path: '/student', component: Student, name: 'student'},
    { path: '/student', component: Student, name: 'student_view'},
    { path: '/student/new', component: StudentCreate, name: 'student_create'},
    { path: '/student/edit/:id', component: StudentEdit, name: 'student_edit'},
    { path: '/course', component: Course, name: 'course'},
    { path: '/course/:id', component: CourseView, name: 'course_view'},
    { path: '/course/new', component: CourseCreate, name: 'course_create'},
    { path: '/course/edit/:id', component: CourseEdit, name: 'course_edit'},
    { path: '/test', component: Test, name: 'test'},
    { path: '/test/:id', component: TestView, name: 'test_view'},
    { path: '/test/new', component: TestCreate, name: 'test_create'},
    { path: '/test/edit/:id', component: TestEdit, name: 'test_edit'}

];
