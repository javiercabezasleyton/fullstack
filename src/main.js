import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import VueSweetAlert from 'vue-sweetalert'

import { routes } from './routes'
import 'bulma'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(VueSweetAlert);

Vue.config.productionTip = false;

Vue.component('icon', Icon);
export const router = new VueRouter({
    routes
});

Vue.mixin({
    data: function() {
        return {
            get url_backend() {
                return "http://127.0.0.1:8000/";
            }
        }
    }
});

new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
});
